<%@ page import="java.time.DayOfWeek" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.Month" %><%--
  Created by IntelliJ IDEA.
  User: yab2
  Date: 10/01/2019
  Time: 10:58 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>exercise01-01</title>
    </head>
    <body>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tincidunt, nulla in maximus congue, lorem
            velit porttitor massa, nec hendrerit massa eros et enim. Nunc metus sem, maximus eget velit vitae, accumsan
            dictum felis. Phasellus quis neque consequat, fringilla erat eu, ultrices augue. Morbi varius risus
            efficitur augue tincidunt commodo. Pellentesque semper odio id dui mattis, quis elementum ex auctor. In
            facilisis hendrerit ultrices. Suspendisse ex odio, ultrices a pretium eget, ultrices sed felis. Donec
            tristique ante eu justo posuere congue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
            posuere cubilia Curae; Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>

        <p>Etiam vel mi vel metus iaculis laoreet. Cras sit amet porta quam. Sed lobortis purus vitae dolor imperdiet
            faucibus. Nulla porta leo vel mauris ultrices scelerisque. Donec tempus neque lacus, sit amet condimentum
            sapien venenatis eu. Integer nec orci et nibh aliquet luctus at in felis. Vivamus a vulputate purus, nec
            aliquam ipsum. Vivamus vitae facilisis leo, eget rutrum tellus.</p>

        <p>In aliquet non libero id accumsan. Morbi sodales mauris dictum consectetur imperdiet. Donec finibus lorem
            quis metus fermentum, eget lacinia magna convallis. Ut ac neque sed nunc fermentum venenatis. Mauris nec
            neque efficitur, imperdiet ipsum eu, euismod est. Cras vel ligula mattis, pharetra felis at, facilisis
            nulla. Cras vel euismod lectus. Sed cursus, diam et ullamcorper tincidunt, nisi dui eleifend enim, sit amet
            laoreet est dolor vel quam. Aliquam sed commodo risus. Fusce sapien elit, molestie sit amet ante a, ultrices
            sollicitudin nunc. Maecenas fringilla odio eget massa aliquam condimentum. Donec sit amet ligula quis magna
            congue porta vel vel leo. Sed nec egestas turpis. Nullam rutrum, tellus eu dignissim rutrum, felis dolor
            suscipit mauris, id egestas nisi sapien at augue.</p>

        <p>Donec ac dignissim felis. Etiam dignissim diam massa. Donec bibendum sem sed magna condimentum tempus. In mi
            ante, ornare a tellus vel, egestas tincidunt dolor. Suspendisse non elit finibus, aliquam elit at, viverra
            ligula. Phasellus a elementum velit. Cras molestie nulla non vestibulum feugiat. Quisque consequat efficitur
            fringilla. Aliquam semper venenatis turpis. Sed accumsan pellentesque velit, id pulvinar lorem pulvinar ut.
            Integer nec finibus ex. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
            Curae; Quisque volutpat augue leo, nec pulvinar turpis aliquet et.</p>

        <p>Proin vel mauris eleifend, hendrerit risus eleifend, dignissim tortor. Etiam leo nisi, consectetur eget
            pellentesque non, volutpat quis ex. Donec eu accumsan est. In at leo vitae enim lacinia lacinia. Nulla
            sapien urna, gravida id volutpat et, aliquet vel est. Nullam vitae euismod velit, vel malesuada elit.
            Phasellus non porta nisi, eu volutpat dolor. Maecenas at fermentum leo.</p>

        <hr>

        <%@ include file="exercise02-date.jsp"%>
    </body>
</html>
