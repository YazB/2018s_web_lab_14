<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.DayOfWeek" %>
<%@ page import="java.time.Month" %><%--
  Created by IntelliJ IDEA.
  User: yab2
  Date: 10/01/2019
  Time: 12:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>exercise02-date</title>
    </head>
    <body>
        <%!
            public DayOfWeek getDayOfWeek() {
                return LocalDateTime.now().getDayOfWeek();
            }

            public Month getMonth() {
                return LocalDateTime.now().getMonth();
            }
            public int getDayOfMonth() {
                return LocalDateTime.now().getDayOfMonth();
            }
            public int getYear() {
                return LocalDateTime.now().getYear();
            }
        %>

        <p>Today is <%= getDayOfWeek() %> the <%=getDayOfMonth()%>th of <%=getMonth()%> <%=getYear()%></p>
    </body>
</html>
