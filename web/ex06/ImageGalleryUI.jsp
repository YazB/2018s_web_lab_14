<%--
  Created by IntelliJ IDEA.
  User: yab2
  Date: 11/01/2019
  Time: 1:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
    </head>
    <body>
        <table style="width:100%">
            <thead>
                <tr>
                    <th>Thumbnail</th>
                    <th><a href='ImageGalleryDisplay?sortColumn=filename&order=" + filenameSortToggle+ "ending'>Filename <img src='images/sort-" + currFilenameSortToggle + ".png' alt='icon' /></a></th>
                    <th><a href='ImageGalleryDisplay?sortColumn=filesize&order=" + filesizeSortToggle+ "ending'>File-size <img src='images/sort-" + currFilesizeSortToggle + ".png'  alt='icon' /></a></th>
                </tr>
            </thead>
        <p>Path was ${photosPath}</p>
            <tr>
        <c:forEach items="${files}" var="file">
            <td><a href="./Photos/${file.fullFile}"><img width="100%" src="./Photos/${file.fullFile}"></a></td>
            <td>${file.thumbDisplay}</td>
            <td>${file.fullfileSize}</td>
            </tr>
        </c:forEach>
        </table>
    </body>
</html>