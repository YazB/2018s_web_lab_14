package ictgradschool.ictgradschool.web.lab14.imagegallery;

import java.io.File;
import java.io.Serializable;

public class FileInfo implements Serializable {
    File thumbPath;
    long fullfileSize;
    File fullFile;
    String thumbDisplay;

    public File getThumbPath() {
        return thumbPath;
    }

    public void setThumbPath(File thumbPath) {
        this.thumbPath = thumbPath;
    }

    public long getFullfileSize() {
        return fullfileSize;
    }

    public void setFullfileSize(long fullfileSize) {
        this.fullfileSize = fullfileSize;
    }

    public File getFullFile() {
        return fullFile;
    }

    public void setFullFile(File fullFile) {
        this.fullFile = fullFile;
    }

    public String getThumbDisplay() {
        return thumbDisplayString();
    }

    public void setThumbDisplay() {
        return; // NOOP
    }

    public FileInfo() {
    }

    public FileInfo(File thumbfile, long fullfileSize, File fullFile) {
        this.thumbPath = thumbfile;
        this.fullfileSize = fullfileSize;
        this.fullFile = fullFile;
        this.thumbDisplay = thumbDisplayString();
    }

    // return a tidy display string for the thumbnail name
    private String thumbDisplayString() {
        String displayStr = thumbPath.getName(); // path removed
        displayStr = displayStr.replace(ImageGalleryDisplay.THUMBNAIL_SUFFIX, ""); // get rid of "_thumbnail.png" suffix
        displayStr = displayStr.replace('_', ' '); // _ replaced with space
        return displayStr;
    }

}
