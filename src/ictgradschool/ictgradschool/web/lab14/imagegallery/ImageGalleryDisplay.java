package ictgradschool.ictgradschool.web.lab14.imagegallery;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Servlet implementation class ImageGalleryDisplay
 */
public class ImageGalleryDisplay extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private String photosPath;
    //Set in response after here

    static final String THUMBNAIL_SUFFIX = "_thumbnail.png";

    private final static int COOKIE_EXPIRATION_TIME = 5 * 60; // cookies stored for 5 mins (5*60 seconds) which suffices for testing

    // don't make the following into class variables, as they are a single user's settings, whereas this
    // Servlet class should work for all users that may connect to it, each with their own user preferencse
    //private String filenameSortToggle = "asc";
    //private String filesizeSortToggle = "asc";

    // servletContext() only accessible from init(), not from constructor 
    public void init() {
        photosPath = getServletContext().getRealPath("Photos");
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //response.getWriter().append("Served at: ").append(request.getContextPath());

        HttpSession session = request.getSession(); // this will create a session if one doesn't exist.
        // This step needs to be done before any output to the page
        // we'll be storing sort order constraints in the session in the function doSortDisplay()

        PrintWriter out = response.getWriter();

//        out.println("<!DOCTYPE html>");
//        out.println("<html>");
//        out.println("<head>");
//        out.println("<meta charset='UTF-8' />");
//        out.println("<title>Image gallery display</title>");
//        out.println("</head>\n<body>");

//        out.println("<p>Photos path: " + photosPath + "</p>");
        File photosFolder = new File(photosPath);

        if (!photosFolder.exists()) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Photos folder"+ photosPath + " does not exist.");
//            out.println("<p>Photos folder " + photosPath + " does not exist.</p>");
//            out.println("</body>\n</html>");
            return;
        } else if (!photosFolder.canRead()) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Photos folder" + photosPath + " can't be read");
//            out.println("<p>Photos folder " + photosPath + " can't be read.</p>");
//            out.println("</body>\n</html>");
            return;
        }

        request.setAttribute("photosPath", photosPath);

        File[] files = photosFolder.listFiles(); // File objects returned can be folders
        List <FileInfo> fileDataList = new LinkedList <FileInfo>();



        File thumbPath = null;
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                String filename = files[i].getName();

                if (filename.endsWith(THUMBNAIL_SUFFIX)) {
                    //thumbPath = files[i];
                    continue;
                } else if (filename.endsWith(".jpg") || filename.endsWith(".png") || filename.endsWith(".gif")) {

                    long fullfileSize = files[i].length();

                    // found full size image
                    // try to find matching thumbnail file for the image
                    String parentFolder = files[i].getParent();
                    String fileNamePrefix = filename.substring(0, filename.lastIndexOf("."));
                    thumbPath = new File(parentFolder, fileNamePrefix + THUMBNAIL_SUFFIX);
                    if (thumbPath.exists()) {
                        fileDataList.add(new FileInfo(thumbPath, fullfileSize, files[i]));
                    } else {
                        out.println("Could not find thumbnail " + thumbPath.getName() + " for image " + filename);
                        fileDataList.add(new FileInfo(null, fullfileSize, files[i]));
                    }
                }
            }
        }


        if (fileDataList.size() == 0) {
            out.println("<p>No images found in Photos folder " + photosPath + ".</p>");
        } else {

            // Get filenameSortToggle, filesizeSortToggle sort orders, if stored. Else store their initial values as ascending
            String filenameSortToggle, filesizeSortToggle;
            Object fnToggle = session.getAttribute("filenameSortToggle");
            Object fsToggle = session.getAttribute("filesizeSortToggle");

            if (fnToggle == null) { // initialise and store in the session for future use
                filenameSortToggle = "asc";
                session.setAttribute("filenameSortToggle", filenameSortToggle);
            } else {
                filenameSortToggle = (String) fnToggle;
            }

            if (fsToggle == null) { // initialise and store in the session for future use
                filesizeSortToggle = "asc";
                session.setAttribute("filesizeSortToggle", filesizeSortToggle);
            } else {
                filesizeSortToggle = (String) fsToggle;
            }

            String currFilenameSortToggle = filenameSortToggle;
            String currFilesizeSortToggle = filesizeSortToggle;

            // may have been asked to sort the fileDataList. If so, do it now
            doSortDisplay(request, response, fileDataList, out);

            request.setAttribute("files", fileDataList);

            // doSortDisplay() will also have toggled session's filenameSortToggle and filesizeSortToggle to the future state
            filenameSortToggle = (String) session.getAttribute("filenameSortToggle");
            filesizeSortToggle = (String) session.getAttribute("filesizeSortToggle");

            //out.println("*** future: " + filenameSortToggle + " - current: " + currFilenameSortToggle);

            // the sort order toggle image displayed reflects the current state of the sort order
            // but the sort order that is used when clicking on a sorting link is the future (toggled) sort order

//            out.println("<table>");
//            out.println("<tr><th>Thumbnail</th>");
//            out.println("<th><a href='ImageGalleryDisplay?sortColumn=filename&order=" + filenameSortToggle
//                    + "ending'>Filename <img src='images/sort-" + currFilenameSortToggle + ".png' alt='icon' /></a></th>");
//            out.println("<th><a href='ImageGalleryDisplay?sortColumn=filesize&order=" + filesizeSortToggle
//                    + "ending'>File-size <img src='images/sort-" + currFilesizeSortToggle + ".png'  alt='icon' /></a></th>");
//            out.println("</tr>");

//            for (int i = 0; i < fileDataList.size(); i++) {
//                FileInfo fileData = fileDataList.get(i);
//                File thumbNailFile = fileData.thumbPath;
//                if (thumbNailFile == null) continue;
//
//                File fullFile = fileData.getFullFile();
//                long filesize = fileData.fullfileSize;
//                String displayStr = fileData.thumbDisplay;
//
////                out.append("<tr>\n<td>");
////                out.println("<a href='Photos/" + fullFile.getName() + "'>");
////                out.println("<img src='Photos/" + thumbNailFile.getName() + "' alt='" + displayStr + "' />");
////                out.append("</a>");
////                out.append("</td>\n<td>");
////                out.append(displayStr);
////                out.append("</td>\n<td>");
////                out.print(filesize);
////                out.println("</td>\n</tr>");
//            }
//            out.println("</table>\n");
        }
//        out.println("</body>\n</html>");

        request.getRequestDispatcher("ex06/ImageGalleryUI.jsp").forward(request, response);
    }


    private List <FileInfo> doSortDisplay(HttpServletRequest request, HttpServletResponse response,
                                          List <FileInfo> fileDataList, PrintWriter out) {
        HttpSession session = request.getSession();

        // Check request parameters for any sorting constraints supplied
        String sortColumn = request.getParameter("sortColumn");
        String sortOrder = request.getParameter("order");


        // Check cookies first for any stored sorting constraints
        if (sortColumn == null && sortOrder == null) {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {

                Cookie cookie;

                for (int i = 0; i < cookies.length; i++) {
                    // let's find the cookie
                    cookie = cookies[i];
                    if (cookie.getName().equals("sortColumn")) sortColumn = cookie.getValue();
                    if (cookie.getName().equals("sortOrder")) sortOrder = cookie.getValue();
                }
            }

            if (sortColumn != null && sortOrder != null) {
                out.println("Found sorting cookies: sort " + sortColumn + " by " + sortOrder);
            }

        }

        // Next check the session for any stored sorting constraints
        if (sortColumn == null && sortOrder == null) {
            out.println("Couldn't find sorting constraints in cookies, trying session...");

            sortColumn = (String) session.getAttribute("sortColumn");
            sortOrder = (String) session.getAttribute("sortOrder");

            if (sortColumn != null && sortOrder != null) {
                out.println("Found sorting constraints in session: sort " + sortColumn + " by " + sortOrder);
            }
        }


        // now we may have sorting constraints either from the request parameters, or remembered (from cookies or else the session)

        if (sortColumn != null && sortOrder != null) {
            if ((sortColumn.equals("filename") || sortColumn.equals("filesize"))
                    &&
                    (sortOrder.equals("ascending") || sortOrder.equals("descending"))) {
                // we've been given meaningful sort constraints, so try sorting by them

                Comparator <FileInfo> comparator = new FileInfoComparator(sortColumn, sortOrder);
                Collections.sort(fileDataList, comparator);
            } else {
                out.println("Sorting constraints " + sortColumn + " by " + sortOrder + " not recognised. Not sorting.");

            }

            // having sorted, toggle the sortOrder for future use
            if (sortColumn.equals("filename")) {
                if (sortOrder.equals("ascending")) {
                    session.setAttribute("filenameSortToggle", "desc");
                } else if (sortOrder.equals("descending")) {
                    session.setAttribute("filenameSortToggle", "asc");
                }
            } else if (sortColumn.equals("filesize")) {
                if (sortOrder.equals("ascending")) {
                    session.setAttribute("filesizeSortToggle", "desc");
                } else if (sortOrder.equals("descending")) {
                    session.setAttribute("filesizeSortToggle", "asc");
                }
            }

            out.println("Found sort column: " + sortColumn + " by " + sortOrder);

            // Finally, since sortColumn and order were defined,
            // we remember these sorting constraints for next time by setting them as cookies and as attributes of the Session

            // Create cookie, set their expiration time and then add them to the response (they go into the response header)
            // so that the browser can then store them on its end
            Cookie sortColumnCookie = new Cookie("sortColumn", sortColumn);
            sortColumnCookie.setMaxAge(COOKIE_EXPIRATION_TIME);
            response.addCookie(sortColumnCookie);

            Cookie sortOrderCookie = new Cookie("sortOrder", sortOrder);
            sortOrderCookie.setMaxAge(COOKIE_EXPIRATION_TIME);
            response.addCookie(sortOrderCookie);


            session.setAttribute("sortColumn", sortColumn);
            session.setAttribute("sortOrder", sortOrder);
        }

        return fileDataList;
    }
}
