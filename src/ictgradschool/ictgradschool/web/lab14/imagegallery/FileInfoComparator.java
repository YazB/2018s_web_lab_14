package ictgradschool.ictgradschool.web.lab14.imagegallery;

import java.util.Comparator;

// http://stackoverflow.com/questions/4108604/java-comparable-vs-comparator
// http://www.tutorialspoint.com/java/java_using_comparator.htm
// No natural ordering for FileInfo, since we can sort by filesize OR filename
// therefore implement Comparator for custom sorting
public class FileInfoComparator implements Comparator<FileInfo> {
    private String sortField;
    private String sortOrder;

    public FileInfoComparator(String sortField, String sortOrder) {
        this.sortField = sortField; // class member variable called 'sortField' is assigned the value of parameter also called 'sortField'
        this.sortOrder = sortOrder;
    }

    public int compare(FileInfo fi1, FileInfo fi2) {

        if (sortField.equals("filename")) {
            if (sortOrder.equals("ascending")) {
                return fi1.thumbDisplay.compareTo(fi2.thumbDisplay);
            } else { // descending
                return fi2.thumbDisplay.compareTo(fi1.thumbDisplay);
            }
        } else if (sortField.equals("filesize")) {
            // can't do return fi1.fullfileSize - fi2.fullfileSize; for ascending sort
            // or return fi2.fullfileSize - fi1.fullfileSize; for descending sort
            // since the type of fullfileSize is long and the return value of this function is int

            // Using the Java Object Wrapper "Long" to wrap basic data type long
            // and then use its object method compareTo() as we did with String.compareTo above

            Long fileSize1 = fi1.fullfileSize;
            Long fileSize2 = fi2.fullfileSize;

            if (sortOrder.equals("ascending")) {
                return fileSize1.compareTo(fileSize2);

            } else { // descending
                return fileSize2.compareTo(fileSize1);
            }
        } else {
            System.err.println("Meaningless comparison of FileInfos on unknown " + sortField + ". Not sorting.");
            return 0;
        }
    }
}